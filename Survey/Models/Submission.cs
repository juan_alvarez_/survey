﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Survey.Models
{
    public class Submission : Base
    {
        public DateTime Completed { get; set; }
        public int SurveyId { get; set; }
        public virtual ICollection<SubmissionAnswer> SubmissionAnswers { get; set; } 
    }
}