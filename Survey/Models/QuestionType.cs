﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Survey.Models
{
    public enum QuestionType
    {
        Text,
        Multiple,
        Single,
        Rating
    }
}