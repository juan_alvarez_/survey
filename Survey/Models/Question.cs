﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Survey.Models
{
    public class Question : Base
    {
        public string Text { get; set; }

        public QuestionType Type { get; set; }

        public int Order { get; set; }

        public int SurveyId { get; set; }

        [ForeignKey("SurveyId")]
        public virtual Survey Survey { get; set; }

        public virtual ICollection<Answer> Answers { get; set; } 
    }
}