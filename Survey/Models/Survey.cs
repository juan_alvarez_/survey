﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Survey.Models
{
    public class Survey : Base
    {
        public string Header { get; set; }

        public virtual ICollection<Question> Questions { get; set; } 
    }
}