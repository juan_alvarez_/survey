﻿using System.Collections.Generic;
using System.Data.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;

namespace Survey.Models
{
    public class SurveyDbInitializer : DropCreateDatabaseIfModelChanges<SurveyContext>
    {
        protected override void Seed(SurveyContext context)
        {
            context.Surveys.Add(new Survey
            {
                Header = "Customer Satisfaction Survey Template",
                Questions = new List<Question> {
                    new Question
                    {
                        Type = QuestionType.Rating, Text = "How likely is it that you would recommend this company to a friend or colleague?", Order = 1,
                        Answers = new List<Answer>
                        {
                            new Answer { Text = "Not at all likely" },
                            new Answer { Text = "Extremely likely" }
                        }
                    },
                    new Question
                    {
                        Type = QuestionType.Single, Text = "Overall, how satisfied or dissatisfied are you with our company?", Order = 2,
                        Answers = new List<Answer>
                        {
                            new Answer { Text = "Very satisfied" },
                            new Answer { Text = "Somewhat satisfied" },
                            new Answer { Text = "Neither satisfied nor dissatisfied" },
                            new Answer { Text = "Somewhat dissatisfied" },
                            new Answer { Text = "Very dissatisfied" }
                        }
                    },
                    new Question
                    {
                        Type = QuestionType.Multiple, Text = "Which of the following words would you use to describe our products? Select all that apply.",  Order = 3,
                        Answers = new List<Answer>
                        {
                            new Answer { Text = "Reliable" },
                            new Answer { Text = "High quality" },
                            new Answer { Text = "Useful" },
                            new Answer { Text = "Unique" },
                            new Answer { Text = "Good value for money" },
                            new Answer { Text = "Overpriced" },
                            new Answer { Text = "Impractical" },
                            new Answer { Text = "Ineffective" },
                            new Answer { Text = "Poor quality" },
                            new Answer { Text = "Unreliable" }
                        }
                    },
                    new Question
                    {
                        Type = QuestionType.Single, Text = "How well do our products meet your needs?",  Order = 4,
                        Answers = new List<Answer>
                        {
                            new Answer { Text = "Extremely well" },
                            new Answer { Text = "Very well" },
                            new Answer { Text = "Somewhat well" },
                            new Answer { Text = "Not so well" },
                            new Answer { Text = "Not at all well" }
                        }
                    },
                    new Question
                    {
                        Type = QuestionType.Single, Text = "How would you rate the quality of the product?",  Order = 5,
                        Answers = new List<Answer>
                        {
                            new Answer { Text = "Very high quality" },
                            new Answer { Text = "High quality" },
                            new Answer { Text = "Neither high nor low quality" },
                            new Answer { Text = "Low quality" },
                            new Answer { Text = "Very low quality" }
                        }
                    },
                    new Question
                    {
                        Type = QuestionType.Single, Text = "How would you rate the value for money of the product?",  Order = 6,
                        Answers = new List<Answer>
                        {
                            new Answer { Text = "Excellent" },
                            new Answer { Text = "Above average" },
                            new Answer { Text = "Average" },
                            new Answer { Text = "Below average" },
                            new Answer { Text = "Poor" }
                        }
                    },
                    new Question
                    {
                        Type = QuestionType.Single, Text = "How responsive have we been to your questions or concerns about our products?",  Order = 7,
                        Answers = new List<Answer>
                        {
                            new Answer { Text = "Extremely responsive" },
                            new Answer { Text = "Very responsive" },
                            new Answer { Text = "Somewhat responsive" },
                            new Answer { Text = "Not so responsive" },
                            new Answer { Text = "Not at all responsive" },
                            new Answer { Text = "Not applicable" }
                        }
                    },
                    new Question
                    {
                        Type = QuestionType.Single, Text = "How long have you been a customer of our company?",  Order = 8,
                        Answers = new List<Answer>
                        {
                            new Answer { Text = "This is my first purchase" },
                            new Answer { Text = "Less than six months" },
                            new Answer { Text = "Six months to a year" },
                            new Answer { Text = "1 - 2 years" },
                            new Answer { Text = "3 or more years" },
                            new Answer { Text = "I haven't made a purchase yet" }
                        }
                    },
                    new Question
                    {
                        Type = QuestionType.Single, Text = "How likely are you to purchase any of our products again?",  Order = 9,
                        Answers = new List<Answer>
                        {
                            new Answer { Text = "Extremely likely" },
                            new Answer { Text = "Very likely" },
                            new Answer { Text = "Somewhat likely" },
                            new Answer { Text = "Not so likely" },
                            new Answer { Text = "Not at all likely" }
                        }
                    },
                    new Question { Type = QuestionType.Text, Text = "Do you have any other comments, questions, or concerns?",  Order = 10 }
                }
            });

            var userManager = ApplicationUserManager.Create(new IdentityFactoryOptions<ApplicationUserManager>(), context);

            var admin = new ApplicationUser
            {
                UserName = "admin@example.com",
                Email = "admin@example.com",
                EmailConfirmed = true
            };
            var password = "VP7s#z6|e";

            userManager.Create(admin, password);

            base.Seed(context);
        }
    }

    public class SurveyContext : IdentityDbContext<ApplicationUser>
    {
        public SurveyContext() : base("SurveyContext") 
        {
            Database.SetInitializer(new SurveyDbInitializer());

        }

        public static SurveyContext Create()
        {
            return new SurveyContext();
        }

        public DbSet<Survey> Surveys { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Answer> Answers { get; set; }
        public DbSet<Submission> Submissions { get; set; }
        public DbSet<SubmissionAnswer> SubmissionAnswers { get; set; }
    }
}