﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Survey.Models;

namespace Survey.Services
{
    public interface ISurveyService : IDisposable
    {
        Models.Survey GetSurvey(int id);

        void CreateSubmission(Submission submission);

        IEnumerable<Submission> GetSurveySubmissions(int id);

        string GetSurveyTitle(int surveyId);

        string GetQuestionText(int questionId);
    }

    public class SurveyService : ISurveyService
    {
        private readonly SurveyContext _context;

        public SurveyService(SurveyContext context = null)
        {
            _context = context ?? new SurveyContext();
        }

        public void Dispose()
        {
            _context?.Dispose();
        }

        public Models.Survey GetSurvey(int id)
        {
            return _context.Surveys.FirstOrDefault();
        }

        public void CreateSubmission(Submission submission)
        {
            _context.Submissions.Add(submission);
            _context.SaveChanges();
        }

        public IEnumerable<Submission> GetSurveySubmissions(int surveyId)
        {
            return _context.Submissions.Where(x => x.SurveyId == surveyId);
        }

        public string GetSurveyTitle(int surveyId)
        {
            return _context.Surveys.Find(surveyId).Header;
        }

        public string GetQuestionText(int questionId)
        {
            return _context.Questions.Find(questionId).Text;
        }
    }
}