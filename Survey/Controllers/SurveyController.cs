﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Survey.Models;
using Survey.Services;
using Survey.ViewModels;

namespace Survey.Controllers
{
    public class SurveyController : Controller
    {
        private ISurveyService _surveyService;

        public SurveyController()
        {
        }

        public SurveyController(ISurveyService surveyService = null)
        {
            _surveyService = surveyService ?? new SurveyService();
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            _surveyService = new SurveyService();
        }

        // GET: Survey
        public ActionResult View(int id = 1)
        {
            var survey = _surveyService.GetSurvey(id);
            return View(survey);
        }

        [HttpPost]
        public ActionResult SubmitSurvey(SurveyAnswer submission)
        {
            //if (form.HasKeys() && form["Id"] != null && int.TryParse(form["Id"], out surveyId))
            //{
            //    var submission = new Submission
            //    {
            //        SurveyId = surveyId,
            //        Completed = DateTime.UtcNow,
            //        SubmissionAnswers = new List<SubmissionAnswer>()
            //    };

            //    foreach (var key in form.AllKeys)
            //    {
            //        int questionId;
            //        var question = key.Split('.');
            //        if (question.Length != 3 || question[0] != "Question" || !int.TryParse(question[2], out questionId))
            //            continue;

            //        int answerId;
            //        switch (question[1])
            //        {
            //            case "Single":
            //                if (!int.TryParse(form[key], out answerId))
            //                    continue;

            //                submission.SubmissionAnswers.Add( new SubmissionAnswer
            //                {
            //                    QuestionId = questionId,
            //                    AnswerId = answerId
            //                });

            //                break;
            //            case "Multiple":
            //                var answers = form[key].Split(',');
            //                foreach (var answer in answers)
            //                {
            //                    if (!int.TryParse(answer, out answerId))
            //                        continue;

            //                    submission.SubmissionAnswers.Add(new SubmissionAnswer
            //                    {
            //                        QuestionId = questionId,
            //                        AnswerId = answerId
            //                    });
            //                }
            //                break;
            //            case "Rating":
            //            case "Text":
            //                submission.SubmissionAnswers.Add(new SubmissionAnswer
            //                {
            //                    QuestionId = questionId,
            //                    AnswerText = form[key]
            //                });
            //                break;
            //        }
            //    }

            //    if (submission.SubmissionAnswers.Any())
            //        _surveyService.CreateSubmission(submission);
            //}
            return View("ThankYou");
        }

        [Authorize]
        public ActionResult Dashboard(int id = 1)
        {
            var submissions = _surveyService.GetSurveySubmissions(id).ToList();

            if (submissions.Any())
            {
                ViewBag.SurveyName = _surveyService.GetSurveyTitle(submissions.First().SurveyId);

                ViewBag.TotalSubmissions = submissions.Count();

                var ratings = submissions.SelectMany(x => x.SubmissionAnswers).Where(x => x.Question.Type == QuestionType.Rating).ToList();
                ViewBag.Ratings = ratings.Sum(x => float.Parse(x.AnswerText))/ratings.Count();

                var questions = submissions.SelectMany(x => x.SubmissionAnswers).GroupBy(x=>x.QuestionId).OrderByDescending(x => x.Count());
                ViewBag.MostAnswered = _surveyService.GetQuestionText(questions.First().Key);
            }
            else
            {
                ViewBag.SurveyName = "No submissions found";
            }
            

            return View(submissions);
        }
    }
}