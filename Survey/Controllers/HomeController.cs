﻿using System;
using System.Web.Mvc;
using Survey.Models;

namespace Survey.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return RedirectToAction("View", "Survey", new { id = 1 });
        }
    }
}