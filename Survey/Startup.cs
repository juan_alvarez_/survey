﻿using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Owin;
using Survey.Models;
using Survey.Services;

[assembly: OwinStartupAttribute(typeof(Survey.Startup))]
namespace Survey
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
