﻿using System.Collections.Generic;

namespace Survey.ViewModels
{
    public class SurveyAnswer
    {
        public int SurveyId { get; set; }
        public IEnumerable<QuestionAnswer> QuestionAnswers { get; set; }
    }
}