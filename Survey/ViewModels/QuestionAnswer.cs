﻿using System.Collections;
using System.Collections.Generic;
using Survey.Models;

namespace Survey.ViewModels
{
    public class QuestionAnswer
    {
        public QuestionType Type { get; set; }
        public int QuestionId { get; set; }
        public int AnswerId { get; set; }
        public string AnswerText { get; set; }
        public IEnumerable<string> SelectedAnswerIds { get; set; } 
    }
}